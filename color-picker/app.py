from __future__ import unicode_literals
from ColorPicker import ColorPicker
from flask import render_template, jsonify, request
import numpy as np
import pandas as pd
import string

app = ColorPicker(__name__)
app.debug = True
color_hexa = pd.read_csv('color-picker/static/datasets/colorhexa_com.csv')
color_wikipedia = pd.read_csv('color-picker/static/datasets/wikipedia_color_names.csv')
color_wikipedia_x11 = pd.read_csv('color-picker/static/datasets/wikipedia_x11_colors.csv')


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/color', methods=["POST"])
def color():
    code = request.json['code']
    if is_color_code(code):
        color_rgb = hex2rgb(code)
        color_answer = get_color_name(color_rgb[0], color_rgb[1], color_rgb[2]).to_json()
        inverted_color = get_invert_color(color_rgb[0], color_rgb[1], color_rgb[2])
        app.logger.info(inverted_color)
        return jsonify({'color': color_answer, 'inverted_color': inverted_color})
        #return jsonify({'color': color_answer})

    else:
        return jsonify({'error': 'The hexadecimal color is not valid'}), 400


@app.errorhandler(401)
@app.errorhandler(404)
@app.errorhandler(500)
def real_404(error):
    return "Real 404 page", 404


# Rename columns for better readability
def rename_columns():
    # Wikipedia colors are written as 'ColorName' !'ColorName'
    color_wikipedia_x11['Name'] = color_wikipedia_x11['Name'].str.split('!').str[1]
    color_hexa.rename(columns={'Hex (24 bit)': 'Hex',
                               'Red (8 bit)': 'Red',
                               'Green (8 bit)': 'Green',
                               'Blue (8 bit)': 'Blue',
                               'Hue (degrees)': 'Hue',
                               'HSL.S (%)': 'HSL.S',
                               'HSL.L (%)': 'HSL.L'}, inplace=True)
    color_wikipedia.rename(columns={'Hex (24 bit)': 'Hex',
                                    'Red (8 bit)': 'Red',
                                    'Green (8 bit)': 'Green',
                                    'Blue (8 bit)': 'Blue',
                                    'Hue (degrees)': 'Hue',
                                    'HSL.S (%)': 'HSL.S',
                                    'HSL.L (%)': 'HSL.L'}, inplace=True)
    color_wikipedia_x11.rename(columns={'Hex (24 bit)': 'Hex',
                                        'Red (8 bit)': 'Red',
                                        'Green (8 bit)': 'Green',
                                        'Blue (8 bit)': 'Blue',
                                        'Hue (degrees)': 'Hue',
                                        'HSL.S (%)': 'HSL.S',
                                        'HSL.L (%)': 'HSL.L'}, inplace=True)


# Returns the distance between 2 colors
def euclidean_distance(color1, color2):
    return np.sqrt(np.sum(np.subtract(color1, color2) ** 2, axis=1))


# Returns the name of the closest color
def get_color_name(red, green, blue):
    test_color = pd.DataFrame([[red, green, blue]], columns=['Red', 'Green', 'Blue'])
    result_index = euclidean_distance(color_wikipedia_x11[['Red', 'Green', 'Blue']], test_color).idxmin()
    return color_wikipedia_x11.loc[result_index]

'''
    get_invert_color takes red, green and blue color code and returns the inverted color as an array
'''
def get_invert_color(red, green, blue):
    red = 255 - red
    green = 255 - green
    blue = 255 - blue
    return rgb2hex([red, green, blue])

'''
  isHex returns True if the string number is a hexadecimal number. Else False
'''
def ishex(number):
    return all(c in string.hexdigits for c in number)


'''
  is_color_code returns True if the string number is a 6 digits hexadecimal number. Otherwise it returns False
'''
def is_color_code(number):
    if ishex(number) and len(number) == 6:
        return True
    return False

def hex2rgb(hex):
    # Slice hex string every 2 character and convert the substrings into an int
    rgb = [int(hex[i : i + 2], 16) for i in range(0, len(hex), 2)]
    return rgb

'''
    rgb2hex takes an array with red, green and blue color code in it. It returns an hexadecimal string
'''
def rgb2hex(rgb):
    return ''.join('%02x'%i for i in rgb)

if __name__ == "__main__":
    rename_columns()
    app.run(host='0.0.0.0', port=80)
